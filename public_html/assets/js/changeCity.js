$(document).ready(function () {
    
    // function getCookie(name) {
    //   var matches = document.cookie.match(new RegExp(
    //     "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    //   ));
    //   return matches ? decodeURIComponent(matches[1]) : undefined;
    // }

    // function setCookie(name, value, options) {
    //   options = options || {};
    
    //   var expires = options.expires;
    
    //   if (typeof expires == "number" && expires) {
    //     var d = new Date();
    //     d.setTime(d.getTime() + expires * 1000);
    //     expires = options.expires = d;
    //   }
    //   if (expires && expires.toUTCString) {
    //     options.expires = expires.toUTCString();
    //   }
    
    //   value = encodeURIComponent(value);
    
    //   var updatedCookie = name + "=" + value;
    
    //   for (var propName in options) {
    //     updatedCookie += "; " + propName;
    //     var propValue = options[propName];
    //     if (propValue !== true) {
    //       updatedCookie += "=" + propValue;
    //     }
    //   }
    
    //   document.cookie = updatedCookie;
    // }

    function changeInfo(city, prevCity) {
        if (city === 'Астрахань') {
            $('.phone').html('(8512) 51 05 37<br>8 903 349 16 65');
            $('.footer-text_changeable').html('г. Астрахань, ул. Бабушкина, 60 (4 кабинет, 2-й этаж)<br>Телефон: 8-903-349-16-65<br>Факс: (8512) 51-05-37<br>E-mail: enservplus@mail.ru');
            // $('.contacts-page-link').attr("href", "http://enservplus.ru/contacts");
        }

        if (city === 'Красноярск') {
            $('.phone').html('8 965 908 89 89<br>8 965 909 11 99');
            $('.footer-text_changeable').html('г. Красноярск, ул. Железнодорожников, 17<br>Тел.: 8-965-908-89-89, 8-965-909-11-99<br>E-mail: enservplus@mail.ru, yaguseva@mail.ru');
            // $('.contacts-page-link').attr("href", "http://enservplus.ru/kr-contacts");
        }
        
        if (city !== prevCity && document.location.pathname === '/contacts') {
            setTimeout(function() {
                document.location.reload(true);
            }, 200);
        }
    }

	$('#cities').click(function () {
		$('#cities').toggleClass('active');
	});

	$('.dropdown li a').click(function (event) {
	    event.preventDefault();
	    event.stopPropagation();
	    var prevCity = $('.current-city').text();
		$('.current-city').text(event.currentTarget.textContent);
		$.post(
          '/setcookie.php',
          {city: event.currentTarget.textContent},
          function(data) {
            console.log(data);
          }
        );
		changeInfo(event.currentTarget.textContent, prevCity);
		$('#cities').removeClass('active');
// 		var cityName = '';
// 		if (event.currentTarget.textContent === 'Астрахань') cityName = 'astrakhan';
// 		if (event.currentTarget.textContent === 'Красноярск') cityName = 'krasnoyarsk';

        // setCookie('city', event.currentTarget.textContent, {expires: 864000});
	});
	
    // var currentCity = getCookie('city');
    // if (currentCity) {
    //     $('.current-city').text(currentCity);
    //     changeInfo(currentCity);
    // }
	
});
