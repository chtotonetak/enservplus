$(document).ready(function () {
	$(".feedback-btn").leanModal({
		top: 30
	});
	$(".success-btn").leanModal({
		top: 70
	});
	//закрытие модали с формой и открытие модали благодарности
	function toggle_modals() {
	  $("#lean_overlay").fadeOut(200);
	  $("#feedback-modal").css({ "display" : "none" });
	  $("#lean_overlay").fadeIn(200);
	  $(".success-btn").click();
	}
  $(".license-slider").owlCarousel({
    items: 1,
    itemsDesktop: false,
    itemsDesktopSmall: false,
    itemsTablet: false,
    itemsMobile: false,
    center: true,
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: true,
    loop: true,
    autoWidth: true,
    margin: 50
  });
	//отображение кнопки "наверх"
  $(window).scroll(function(){
  	if ($(this).scrollTop() > 400) {
  		$(".scroll-top").fadeIn();
  	} else {
  		$(".scroll-top").fadeOut();
  	}
  });
  //скрол наверх по нажатию на кнопку
  $(".scroll-top").click(function(){
  	$("html, body").animate({ scrollTop: 0 }, 600);
  	return false;
  });
	//отправка формы
	$(".feedback-form").submit(function() {
		var errors = false;
		var form = $(this);

		form.find("input, textarea").each(function() {
			if( $.trim( $(this).val() ) == '' && $(this).attr("required") ) {
				errors = true;
			}
		});

		if( !errors ) {
			var data = form.serialize();
			$.ajax({
				url: 'http://enservplus.ru/send.php',
				type: 'POST',
				data: data,
				beforeSend: function() {
					$(".form-submit").html("Отправка...");
		  		console.log('Отправляю...');
				},
				success: function(res) {
					if( res == 1 ) {
						console.log("Заявка отправлена");
						//очистить поля
						form.find('input:not(#submit), textarea').val('');
						//переключить модали
						/*$(".feedback-form").html(
							'<div class="triangle"></div>' +
			        '<div class="logo"><img src="http://enservplus.ru/images/white_logo.png" alt="лого" class="form-logo"></div>' +
							'<p class="form-text-top" style="font-size: 24px;">Благодарим за оставленную заявку!</p>'
						);*/
						toggle_modals();
					} else {
						console.log('Ошибка отправки' + res);
					}
				},
				error: function() {
		  		//alert('Ошибка!');
				}
			});
		} else {
			$(".form-submit").html("Ошибка!");
		}
		//конец обработки
		return false;
	});
	//кнопка меню
	$(".menu-btn.mobile").click(function () {
		if ( !$(this).hasClass("active") ) {
			$("#menu-for-mobile").slideDown();
			$(this).addClass("active");
		} else {
			$("#menu-for-mobile").slideUp();
			$(this).removeClass("active");
		}
	});
	
})
