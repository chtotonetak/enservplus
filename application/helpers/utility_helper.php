<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function asset_url() {
    echo base_url() . 'assets';
}

function get_pages_titles() {
    return $pages_titles = array(
        base_url() => 'Главная',
        base_url() . 'home' => 'Главная',
        base_url() . 'about' => 'О компании',
        base_url() . 'service' => 'Услуги',
        base_url() . 'energoaudit' => 'Энергоаудит',
        base_url() . 'sertification' => 'Сертификация систем менеджмента',
        base_url() . 'electromeasure' => 'Электроизмерительные работы',
        base_url() . 'askue' => 'Учет электроэнергии',
        base_url() . 'fire-safety' => 'Пожарная безопасность',
        base_url() . 'maintenance' => 'Техническое обслуживание электрооборудования',
        base_url() . 'planning' => 'Проектирование',
        base_url() . 'electric-works' => 'Электромонтажные и пусконаладочные работы',
        base_url() . 'education' => 'Обучение',
        base_url() . 'license' => 'Лицензии',
        base_url() . 'reply' => 'Отзывы',
        base_url() . 'contacts' => 'Контакты'
    );
}
