<?php
/* defined('BASEPATH') OR exit('No direct script access allowed'); */

function isText($it) {
    return $it['slug'] === '#';
}

function hasChildren($it) {
    return !empty($it['children']);
}

function render_item($menu_item) {
    $item = '<li class="menu-item %s">%s</li>';
    $link = '<a href="%s">%s</a>';
    $text = '<span>%s</span>';
    $nest = '<ul class="menu-dropdown">%s</ul>';

    if ( !isset($menu_item['class']) ) {
        $menu_item['class'] = '';
    }

    $ci =& get_instance();
    if ( $ci->uri->segment(1) === $menu_item['slug'] ) {
        $menu_item['class'] .= ' active';
    }
    if ( hasChildren($menu_item) && isText($menu_item) ) {
        $menu_item['class'] .= ' menu-item-text';
    } 

    $html = isText($menu_item) ? $text : $link;
    $attr = array();
    if ( !isText($menu_item) ) {
        /* array_push($attr, base_url() . $menu_item['slug']); */
        array_push($attr, $menu_item['slug']);
    }
    array_push($attr, $menu_item['title']);
    $html = vsprintf($html, $attr);

    $dropdown = '';
    if ( hasChildren($menu_item) ) {
        foreach ( $menu_item['children'] as $child ) {
            $dropdown .= render_item($child);
        }
    }

    if ( !empty($dropdown) ) {
        $html .= sprintf($nest, $dropdown);
    }

    return sprintf($item, $menu_item['class'], $html);
}

function get_menu_items() {
    $menu_routes = array(
        array(
            'slug' => 'about',
            'title' => 'О компании',
        ),
        array(
            'slug' => '#',
            'title' => 'Услуги',
            'children' => array(
                array(
                    'slug' => 'service',
                    'title' => 'Услуги',
                ),
                array(
                    'slug' => 'objects',
                    'title' => 'Завершенные объекты',
                ),
            ),
        ),
        array(
            'slug' => 'education',
            'title' => 'Обучение',
        ),
        array(
            'slug' => 'license',
            'title' => 'Лицензии',
        ),
        array(
            'slug' => 'reply',
            'title' => 'Отзывы',
        ),
        array(
            'slug' => 'contacts',
            'title' => 'Контакты',
        ),
    );

    foreach ($menu_routes as $menu_item) {
        echo render_item($menu_item);
    }
}

