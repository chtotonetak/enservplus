<footer class="main-footer">
    <div class="container">
        <div class="left-column">
            <p class="footer-text">&copy; ООО «Энергосервис+»</p>
            <ul class="social-networks">
                <li class="social-icon vk"><a href="#"></a></li>
                <li class="social-icon fb"><a href="#"></a></li>
                <li class="social-icon ok"><a href="#"></a></li>
            </ul>
        </div>
        <div class="right-column">
            <p class="footer-text footer-text_changeable">
                <?php
                if( isset($_COOKIE["city"]) && $_COOKIE["city"] === 'Красноярск') {
                    echo 'г. Красноярск, ул. Железнодорожников, 17<br>Тел.: 8-965-908-89-89, 8-965-909-11-99<br>E-mail: enservplus@mail.ru, yaguseva@mail.ru';
                } else {
                    echo 'г. Астрахань, ул. Бабушкина, 60 (4 кабинет, 2-й этаж)<br>Телефон: 8-903-349-16-65<br>Факс: (8512) 51-05-37<br>E-mail: enservplus@mail.ru';
                }
                ?>
            </p>
            <a href="<?php base_url(); ?>/doc/politic.pdf" download="Политика конфиденциальности Энергосервис+" class="politic">
                Политика конфиденциальности
            </a>
        </div>
    </div><!-- ./container -->
</footer>
<!-- /// Scripts /// -->
<script src="<?php asset_url(); ?>/js/jquery-3.1.1.min.js"></script>
<script src="<?php asset_url(); ?>/js/jquery-migrate-1.4.1.min.js"></script>
<script src="<?php asset_url(); ?>/js/jquery.leanModal.min.js"></script>
<script src="<?php asset_url(); ?>/js/changeCity.js">></script>
<script src="<?php asset_url(); ?>/js/owl.carousel.min.js"></script>
<script src="<?php asset_url(); ?>/js/lightbox.js"></script>
<script src="<?php asset_url(); ?>/js/main.js?ver=1.2"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter41428114 = new Ya.Metrika({
                    id:41428114,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/41428114" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>