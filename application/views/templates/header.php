<?php 
$arrPagesTitles = get_pages_titles(); // функция из utility_helper.php
$currentPageTitle = '';
foreach ($arrPagesTitles as $key => $value) {
    if ($key === current_url()) $currentPageTitle = $value;
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Энергосервис+</title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php asset_url(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php asset_url(); ?>/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php asset_url(); ?>/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="<?php asset_url(); ?>/manifest.json">
    <link rel="mask-icon" href="<?php asset_url(); ?>/safari-pinned-tab.svg" color="#2463b3">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?php asset_url(); ?>/css/reset.css">
    <link rel="stylesheet" href="<?php asset_url(); ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php asset_url();?>/css/lightbox.css">
    <link rel="stylesheet" href="<?php asset_url(); ?>/css/style.css?ver=1.4">
</head>
<body>
<div class="feedback-form-wrapper" id="feedback-modal" style="display: none">
    <form class="feedback-form">
        <div class="triangle"></div>
        <div class="logo">
            <img src="<?php asset_url(); ?>/images/white_logo.png" alt="лого" class="form-logo">
        </div>
        <p class="form-text-top">Пожалуйста заполните форму для отправки заявки</p>
        <input name="Ф.И.О." type="text" class="form-input" placeholder="Ф.И.О*" required="">
        <input name="Телефон" type="text" class="form-input" placeholder="Телефон*" required="">
        <input name="E-mail" type="text" class="form-input" placeholder="E-mail">
        <textarea name="Сообщение" class="form-textarea" placeholder="Сообщение"></textarea>
        <input name="Страница" type="hidden" value="<?=$currentPageTitle ?>">
        <label for="agree-box" class="agree-box form-text-bottom">
            <input type="checkbox" required id="agree-box">
            Согласен на обработку персональных данных
        </label>
        <p class="form-text-bottom">* поле обязательное к заполнению</p>
        <div class="form-footer">
            <button class="form-submit">Отправить</button>
        </div>
    </form>
</div>
<div class="feedback-form-wrapper" id="success-modal" style="display: none">
    <button class="success-btn" href="#success-modal" style="display: none;">Оставить заявку</button>
    <form class="feedback-form">
        <div class="triangle"></div>
        <div class="logo">
            <img src="<?php asset_url(); ?>/images/white_logo.png" alt="лого" class="form-logo">
        </div>
        <p class="form-text-top" style="font-size: 24px;">Благодарим за оставленную заявку!</p>
    </form>
</div>
<header class="main-header">
    <div class="container">
        <div class="site-logo">
            <a href="/">
                <img src="<?php asset_url(); ?>/images/logo.png" alt="лого" class="logo-img">
            </a>
        </div>
        <nav class="main-menu">
            <ul class="main-menu-wrapper">
                <?php get_menu_items(); ?>
            </ul>
        </nav>
        <div class="header-feedback">
            <div id="cities" class="wrapper-dropdown">
                <?php 
                if( isset($_COOKIE["city"]) ) {
                    echo '<span class="current-city">' . $_COOKIE["city"] . '</span>';
                } else {
                    echo '<span class="current-city">Астрахань</span>';
                }
                ?>
                <ul class="dropdown">
                	<li><a href="#">Астрахань</a></li>
                	<li><a href="#">Красноярск</a></li>
                </ul>
            </div>
            <p class="phone">
                <?php
                if( isset($_COOKIE["city"]) && $_COOKIE["city"] === 'Красноярск') {
                    echo '8 965 908 89 89<br>8 965 909 11 99';
                } else {
                    echo '(8512) 51 05 37<br>8 903 349 16 65';
                }
                ?>
            </p>
        </div>
        <!-- мобильное меню -->
        <nav class="main-menu mobile hidden">
            <button class="menu-btn mobile hidden">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </nav>
    </div><!-- ./container -->
    <ul id="menu-for-mobile" class="main-menu-wrapper">
        <?php get_menu_items(); ?>
    </ul>
</header>

<div class="page-top">
    <div class="text-container">
        <p class="page-top-text">
            ЭНЕРГЕТИКА<br>
            СЕРТИФИКАЦИЯ СИСТЕМ МЕНЕДЖМЕНТА<br>
            ПОЖАРНАЯ БЕЗОПАСНОСТЬ<br>
            ПРОЕКТИРОВАНИЕ<br>
            ОБУЧЕНИЕ
        </p>
    </div>
    <div class="container button-container">
        <button class="feedback-btn" href="#feedback-modal">Оставить заявку</button>
    </div>
</div>
