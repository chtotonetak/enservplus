<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<main class="page-content">
    <div class="container">
        <article class="services">
            <div class="article-check"></div>
            <p class="page-title">Услуги</p>
            <div class="container">
                <div class="row">
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/electromont.jpg" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/electric-works" class="service-link">ЭЛЕКТРОМОНТАЖНЫЕ РАБОТЫ</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/service_3.png" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/electromeasure" class="service-link">ЭЛЕКТРОЛАБОРАТОРИЯ</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/service_1.png" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/energoaudit" class="service-link">ЭНЕРГОАУДИТ</a>
                    </div>
                </div>
                <div class="row">
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/techobsluzhiv.jpg" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/maintenance" class="service-link">ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ ЭЛЕКТРООБОРУДОВАНИЯ</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/proektirovanie.jpg" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/planning" class="service-link">ПРОЕКТИРОВАНИЕ</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/force_tr.jpg" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/trrepair" class="service-link">РЕМОНТ СИЛОВЫХ ТРАНСФОРМАТОРОВ</a>
                    </div>
                </div>
                <div class="row">
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/service_5.png" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/askue" class="service-link">УЧЕТ ЭЛЕКТРОЭНЕРГИИ</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/service_2.png" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/sertification" class="service-link">СЕРТИФИКАЦИЯ<br>
                            СИСТЕМ МЕНЕДЖМЕНТА</a>
                    </div>
                    <div class="service-item">
                        <img src="<?php asset_url(); ?>/images/service_6.png" alt="" class="service-thumb">
                        <a href="<?php base_url(); ?>/fire-safety" class="service-link">ПОЖАРНАЯ БЕЗОПАСНОСТЬ</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</main>
