<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <main class="page-content">
        <div class="container">
            <article class="education">
                <div class="article-check"></div>
                <p class="page-title">Обучение</p>
                <div class="container">
                    <img src="<?php asset_url(); ?>/images/education.jpg" class="photo-column">
                    <p class="margin-text">
                        Учебный Центр ООО «Энергосервис+» (<a href="<?php asset_url(); ?>/images/edu_license.pdf" target="_blank">лицензия</a> на осуществление образовательной деятельности №1987-Б/С от 13.02.2020 г.) предлагает образовательные услуги по следующим программам дополнительного профессионального образования:
                    </p> 
                    <p class="margin-text">
                        1. Обучение по охране труда руководителей и специалистов предприятий, учреждений и организаций.
                    </p>
                    <p class="margin-text">
                        2. Безопасность работ на высоте.
                    </p>
                    <p class="margin-text">
                        3. Безопасность технологических процессов и производств.
                    </p>
                    <p class="margin-text">
                        4. Подготовка мерам пожарной безопасности для руководителей и специалистов предприятий всех форм собственности и лиц, ответственных за пожарную безопасность.
                    </p>
                    <p class="margin-text">
                        5. Пожарно-технический минимум.
                    </p>
                    <p class="margin-text">
                        6. Подготовка электротехнического, неэлектротехнического персонала, персонала тепловых энергоустановок и сетей.
                    </p>
                    <p class="margin-text">
                        7. Обучение в сфере энергосбережения и повышения энергетической эффективности для специалистов, ответственных за энергосбережение организации.
                    </p>
                    <p class="margin-text">
                        8. Профессиональная подготовка (переподготовка) рабочих специальностей.
                    </p>
                    <p class="margin-text">
                        9. Обеспечение экологической безопасности руководителями (специалистами) общехозяйственных систем управления.
                    </p>
                    <p class="margin-text margin-text_25">
                        10. Подготовка ИТР и специалистов ответственных за эксплуатацию опасных производственных объектов.
                    </p>
                    <div class="button-container">
                        <button class="feedback-btn" href="#feedback-modal">Оставить заявку</button>
                    </div>
                </div>
            </article>
        </div><!-- ./container -->
    </main>
