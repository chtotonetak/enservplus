                <div class="article-check"></div>
                <p class="page-title">Контакты</p>
                <div class="top-block">
                    <div class="table fixed">
                        <div class="facade-img table-cell">
                            <img src="<?php asset_url(); ?>/images/facade.jpg" alt="фасад">
                        </div>
                        <div class="text-block table-cell">
                            <p class="text">
                                Город: Астрахань<br>
                                Адрес: 414000, Астрахань, Бабушкина, 60 (4 кабинет, 2-й этаж)<br>
                                Телефон: 8-903-349-16-65<br>
                                Факс: (8512) 51-05-37<br>
                                E-mail: enservplus@mail.ru
                            </p>
                            <button class="feedback-btn" href="#feedback-modal">Оставить заявку</button>
                        </div>
                    </div>
                </div>
                <div class="map">
                    <a class="dg-widget-link" href="http://2gis.ru/astrakhan/firm/1126428188034611/center/48.043532,46.347249/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Астрахани</a><div class="dg-widget-link"><a href="http://2gis.ru/astrakhan/center/48.043532,46.347249/zoom/16/routeTab/rsType/bus/to/48.043532,46.347249╎Энергосервис+, ООО?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Энергосервис+, ООО</a></div><script charset="utf-8" src="http://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"height":414,"borderColor":"#1a1a1a","pos":{"lat":46.347249,"lon":48.043532,"zoom":16},"opt":{"city":"astrakhan"},"org":[{"id":"1126428188034611"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                </div>