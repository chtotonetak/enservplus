                <div class="article-check"></div>
                <p class="page-title">Контакты</p>
                <div class="top-block">
                    <div class="table fixed">
                        <div class="facade-img table-cell">
                            <img src="<?php asset_url(); ?>/images/fasad_kr.jpg" alt="фасад">
                        </div>
                        <div class="text-block table-cell">
                            <p class="text">
                                Город: Красноярск<br>
                                Адрес: 660075, г. Красноярск, ул. Железнодорожников, 17<br>
                                Телефоны: 8-965-908-89-89, 8-965-909-11-99<br>
                                E-mail: enservplus@mail.ru, yaguseva@mail.ru
                            </p>
                            <button class="feedback-btn" href="#feedback-modal">Оставить заявку</button>
                        </div>
                    </div>
                </div>
                <div class="map">
                    <a class="dg-widget-link" href="http://2gis.ru/krasnoyarsk/firm/70000001037507878/center/92.8416395187378,56.0220366881135/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Красноярска</a><div class="dg-widget-link"><a href="http://2gis.ru/krasnoyarsk/center/92.841643,56.021894/zoom/16/routeTab/rsType/bus/to/92.841643,56.021894╎Энергосервис+?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Энергосервис+</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":640,"height":412,"borderColor":"#a3a3a3","pos":{"lat":56.0220366881135,"lon":92.8416395187378,"zoom":16},"opt":{"city":"krasnoyarsk"},"org":[{"id":"70000001037507878"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
                </div>