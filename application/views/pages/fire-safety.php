<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<main class="page-content">
    <div class="container">
        <article class="post">
            <div class="article-check"></div>
            <p class="page-title">
                ПОЖАРНАЯ БЕЗОПАСНОСТЬ
            </p>
            <article class="service-article">
                <br>
                <img src="<?php asset_url(); ?>/images/service_6.png" class="float-left">
                <p class="margin-text">Компания ООО «Энергосервис+» предлагает расширенный спектр услуг в области пожарной безопасности, выполняет деятельность по монтажу, техническому обслуживанию и ремонту средств обеспечения пожарной безопасности зданий и сооружений, допуск к работам - <a href="/assets/images/new_license3_big.jpg" target="_blank">Лицензия МЧС №30-Б/00076 от 24 мая 2017 г.</a> С нашей помощью вы получите качественно выполненные услуги. Мы имеем опыт работы с объектами различной сложности. Наша услуга основана на индивидуальном подходе к каждому клиенту.</p>
                <p class="margin-text">Обеспечивать безопасность вашего объекта – наша основная работа. Мы оказываем любые профильные услуги, от проектирования планов эвакуации и установки сигнализационных систем до регулярной перезарядки огнетушителей. Для нас нет «маленьких» или «больших» заказов, мы стремимся к тому, чтобы вы нашли всю необходимую поддержку в одной организации – а именно, в компании ООО «Энергосервис+».</p>
                <p class="margin-text">Благодаря большому опыту и профессиональным знаниям мы организуем максимально эффективную систему оказания услуг по пожарной безопасности для наших клиентов.</p>
            </article>
        </article>
    </div><!-- ./container -->
</main>
