<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <main class="page-content">
        <div class="container">
            <!-- О компании -->
            <article class="about">
                <div class="article-check"></div>
                <div class="left-column">
                    <p class="title">ООО «Энергосервис+» это:</p>
                    <ul>
                        <li><span>Энергоаудит, разработка энергопаспортов и<br>программ в области энергосбережения</span></li>
                        <li><span>Сертификация систем менеджмента по стандартам ISO</span></li>
                        <li><span>Электротехническая лаборатория</span></li>
                        <li><span>Электромонтажные и пусконаладочные работы</span></li>
                        <li><span>Пожарная безопасность</span></li>
                        <li><span>Проектирование</span></li>
                        <li><span>Техническое обслуживание электрооборудования</span></li>
                        <li><span>Учет электроэнергии</span></li>
                        <li><span>Обучение</span></li>
                    </ul>
                </div>
                <div class="right-column">
                    <img src="<?php asset_url(); ?>/images/about_company_new.png" alt="компания" class="about-img">
                </div>
            </article>
            <div class="container table fixed">
                <!-- Слова директора -->
                <article class="dir-words table-row">
                    <div class="dir-image table-cell">
                        <img src="<?php asset_url(); ?>/images/director2.jpg" alt="директор" class="dir-photo">
                    </div>
                    <div class="dir-text table-cell">
                        <div class="triangle"></div>
                        <p class="text">
                            <span class="red-line"></span>Здравствуйте, уважаемые посетители нашего сайта!<br>
                            <span class="red-line"></span>Вы нашли нас в сети, а это значит, что Вы наш потенциальный заказчик или коллега. Мы рады всем! И всегда готовы помочь!<br>
                            <span class="red-line"></span>Наша организация ООО «Энергосервис+» была создана в г.Астрахань в 2005 году с целью предоставления широкого спектра услуг в области энергетики, сертификации систем менеджмента, предприятиям и организациям различных сфер деятельности.<br>
                            <span class="red-line"></span>За прошедшие 16 лет с момента своего создания наша организация приобрела репутацию профессионального и надежного партнера для многих предприятий и организаций Астраханской области и других регионов Российской Федерации.<br>
                            <span class="red-line"></span>В числе наших клиентов более 500 организаций из нефтегазовой, судостроительной, судоремонтной, химической отраслей, сельскохозяйственной и пищевой промышленности, а также предприятия, оказывающие услуги в сфере строительства, образования, жилищно-коммунального хозяйства и других.
                        </p>
                        <p class="sign">
                            Директор ООО "Энергосервис+"<br>
                            Роман Валентинович Дерюга
                        </p>
                        <img src="<?php asset_url(); ?>/images/sign.jpg" alt="подпись" class="sign-img">
                    </div>
                    <!-- Лицензии -->
                    <article class="license table-cell">
                        <div class="article-check"></div>
                        <div class="title-wrapper">
                            <div class="triangle"></div>
                            <p class="title">Лицензии</p>
                        </div>
                        <div class="license-slider owl-carousel">
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/new_license1_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/new_license1.jpg" alt="лицензия1">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/new_license2_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/new_license2.jpg" alt="лицензия2">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/new_license3_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/new_license3.jpg" alt="лицензия3">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/new_license4-1_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/new_license4-1.jpg" alt="лицензия4">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/new_license5-1_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/new_license5-1.jpg" alt="лицензия5">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/edu_license1_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/edu_license1.jpg" alt="лицензия на обучение">
                                </a>
                            </div>
                            <div class="slide">
                                <a href="<?php asset_url(); ?>/images/edu_license2_big.jpg" data-lightbox="licenses">
                                    <img src="<?php asset_url(); ?>/images/edu_license2.jpg" alt="лицензия на обучение">
                                </a>
                            </div>
                        </div>
                    </article>
                </article>
            </div><!-- ./container -->
            <!-- достоинства -->
            <article class="advantages">
                <p class="title">Наши конкурентные преимущества:</p>
                <ul>
                    <li><span>Качество предоставляемых услуг по адекватным ценам</span></li>
                    <li><span>Честность перед Заказчиком открытость информации и оперативность</span></li>
                    <li><span>Высокий уровень квалификации персонала</span></li>
                    <li><span>Наши специалисты регулярно проходят повышение квалификации и аттестацию</span></li>
                    <li><span>Многолетний опыт оказания услуг</span></li>
                </ul>
                <p class="text">ООО «Энергосервис+» всегда открыто для плодотворного и взаимовыгодного делового сотрудничества!</p>
            </article>
        </div><!-- ./container -->
    </main>
