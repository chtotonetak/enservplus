<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <main class="page-content">
        <div class="container">
            <article class="sertificates">
                <div class="article-check"></div>
                <p class="page-title">Лицензии</p>
                <div class="container">
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/new_license1_big.jpg" data-lightbox="serts">
                                <img style="border: 1px solid #282f39;" src="<?php asset_url(); ?>/images/new_license1.jpg" alt="лицензия">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                Свидетельство<br>
                                СРО ЭНЕРГОАУДИТ
                            </p>
                            <p class="sert-text-mini">
                                Свидетельство о допуске к работам по энергетическому обследованию № 3 – 2017 – 3017043167 – 03 от 14.12.2017
                            </p>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/new_license3_big.jpg" data-lightbox="serts">
                                <img style="border: 1px solid #282f39;" src="<?php asset_url(); ?>/images/new_license3.jpg" alt="лицензия">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                Лицензия МЧС России
                            </p>
                            <p class="sert-text-mini">
                                № 30-Б/00076 от 24.05.2017 на осуществление деятельности по монтажу, техническому обслуживанию и ремонту средств обеспечения пожарной безопасности зданий и сооружений
                            </p>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/sro_full.jpg" data-lightbox="serts">
                                <img style="border: 1px solid #282f39;" src="<?php asset_url(); ?>/images/sro_thumb.jpg" alt="лицензия">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                Выписка из реестра<br>
                                членов саморегулируемой организации
                            </p>
                            <div class="sert-text-mini">
                                Регистрационный номер в государственном реестре саморегулируемых организаций СРО-П-140-27022010<br/>
                                <a style="color: #fff;" href="<?php asset_url(); ?>/images/sro.pdf" target="_blank">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/sv_rtn_full.jpg" data-lightbox="serts">
                                <img style="border: 1px solid #282f39;" src="<?php asset_url(); ?>/images/sv_rtn_thumb.jpg" alt="лицензия">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                СВИДЕТЕЛЬСТВО<br>
                                РОСТЕХНАДЗОРА
                            </p>
                            <div class="sert-text-mini">
                                Свидетельство о регистрации электролаборатории, регистрационный номер № 112 от 28.01.2020<br/>
                                <a style="color: #fff;" href="<?php asset_url(); ?>/images/sv_rtn.pdf" target="_blank">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/edu_licenses_big.jpg" data-lightbox="serts">
                                <img style="border: 1px solid #282f39;" src="<?php asset_url(); ?>/images/edu_license1.jpg" alt="лицензия">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                ЛИЦЕНЗИЯ МИНИСТЕРСТВА ОБРАЗОВАНИЯ И НАУКИ АО
                            </p>
                            <div class="sert-text-mini">
                                №1987-Б/С от 13.02.2020 г. на осуществление образовательной деятельности.<br/>
                                <a style="color: #fff;" href="<?php asset_url(); ?>/images/edu_license.pdf" target="_blank">Подробнее</a>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div><!-- ./container -->
    </main>
