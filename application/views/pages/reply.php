<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<main class="page-content">
        <div class="container">
            <article class="sertificates">
                <div class="article-check"></div>
                <p class="page-title">Отзывы</p>
                <div class="container">
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/reply1_big.jpg" data-lightbox="replies">
                                <img src="<?php asset_url(); ?>/images/reply1.jpg" alt="сертификат">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                ОТЗЫВ АЭРОПОРТ
                            </p>
                            <p class="sert-text-mini">
                                “ОАО “Аэропорт Астрахань” благодарит руководство и коллектив ООО “Энергосервис+” за качественное и своевременное выполнение работ. … “ОАО “Аэропорт Астрахань” рекомендует Общество с ограниченной ответственностью “Энергосервис +” в качестве надежного партнера в области энергосбережения и повышения энергетической эффективности”
                            </p>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/reply5_big.jpg" data-lightbox="replies">
                                <img style="border: 1px solid #000;" src="<?php asset_url(); ?>/images/reply5.jpg" alt="сертификат">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                ОТЗЫВ АПК Астраханский
                            </p>
                            <p class="sert-text-mini">ООО "АПК Астраханский" благодарит ООО "Энергосервис+" за качественное выполнение пуско-наладочных работ</p>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/reply2_big.jpg" data-lightbox="replies">
                                <img src="<?php asset_url(); ?>/images/reply2.jpg" alt="сертификат">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                ОТЗЫВ<br>
                                ВОЛГОТАНКЕР
                            </p>
                            <p class="sert-text-mini">
                                “Работы были выполнены с высоким качеством и в договорные сроки … ЗАО “ССЗ имени Ленина” рекомендует Общество с ограниченной ответственностью “Энергосервис +” в качестве надежного партнера при реализации проектов АСКУЭ”
                            </p>
                        </div>
                    </div>
                    <div class="license">
                        <div class="sert-photo">
                            <a href="<?php asset_url(); ?>/images/reply4_big.jpg" data-lightbox="replies">
                                <img src="<?php asset_url(); ?>/images/reply4.jpg" alt="сертификат">
                            </a>
                        </div>
                        <div class="sert-info">
                            <div class="triangle"></div>
                            <p class="sert-text">
                                ОТЗЫВ АСПО
                            </p>
                            <p class="sert-text-mini">
                                “…Работы выполнены с хорошим качеством и в договорные сроки. ОАО “АСПО” рекомендует Общество с ограниченной ответственностью “Энергосервис+” в качестве надежного партнера при реализации проектов АСКУЭ и энергосбережения”
                            </p>
                        </div>
                    </div>
                </div>
            </article>
        </div><!-- ./container -->
    </main>
    
