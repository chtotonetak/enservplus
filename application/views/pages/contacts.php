<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <main class="page-content">
        <div class="container">
            <article class="contacts">
            <?php
            if (isset($_COOKIE["city"]) && $_COOKIE["city"] === 'Красноярск') {
                include ("kr-contacts.php");
            } else {
                include ("ast-contacts.php");    
			}
			?>
            </article>
        </div><!-- ./container -->
    </main>